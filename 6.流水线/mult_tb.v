/*
模块：流水线乘法器仿真激励文件
作用：测试流水线乘法器，输出结果。
作者：guzijie
日期：2022.2.8
*/

`timescale 1ns/1ns


module mult_tb;

reg 				sys_clk;
reg 				sys_rst_n;
reg 				mult_in_rdy;
reg[3:0]		mult_data1;
reg[3:0]		mult_data2;
wire[7:0] 	mult_result;



initial begin
	#10
	sys_clk = 1'b1;
	sys_rst_n = 1'b1;
	mult_in_rdy  = 1'b1;

	mult_data1=4'd5;
	mult_data2=4'd5;
	#20
	mult_data1=4'd6;
	mult_data2=4'd6;
	#20
	mult_data1=4'd7;
	mult_data2=4'd7;

	
	
end

always #10 sys_clk = ~sys_clk;

wire				mult_out_rdy;
mult
#(
	.M(4),
	.N(4)
)mult_inst
(
	.sys_clk(sys_clk),
	.sys_rst_n(sys_rst_n),
	.in_rdy(mult_in_rdy),
	.mult_data1(mult_data1),
	.mult_data2(mult_data2),
	
	.out_rdy(mult_out_rdy),
	.mult_result(mult_result)
	
);

endmodule