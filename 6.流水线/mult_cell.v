/*
模块：流水线乘法器单元
作用：可被重复例化，构成流水线乘法器主体，实现流水线乘法器，输出结果。
作者：guzijie
日期：2022.2.8
*/


module mult_cell
#(	
	parameter M=4,//假设M≥N
	parameter N=4
)
(
	input 				sys_clk,
	input 				sys_rst_n,
	input 				in_rdy,
	
	input[M+N-1:0]		data1,
	input[N-1:0]		data2,
	input[M+N-1:0] 	acc_data,
	
	output reg[M+N-1:0]	data1_shift,
	output reg[N-1:0]		data2_shift,
	output reg[M+N-1:0]	acc_result,
	output reg				out_rdy
	
);

always@(posedge sys_clk or negedge sys_rst_n) begin
	if(!sys_rst_n) begin
		data1_shift <= 'b0;
		data2_shift <= 'b0;
		acc_result	<= 'b0;
		out_rdy		<= 'b0;
	end
	else if(in_rdy == 1'b1) begin
		out_rdy		<= 1'b1;
		data1_shift	<= data1 << 1;
		data2_shift <= data2 >> 1;
		acc_result	<= data2[0]? acc_data + data1:acc_data;
	end
	else begin
		data1_shift <= 'b0;
		data2_shift <= 'b0;
		acc_result	<= 'b0;
		out_rdy		<= 'b0;
	end
end


endmodule