module AssemblyLineMul
#(
    parameter N=4,
    parameter M=4
)
(
    //输入信号
    //时钟和复位信号
    input clk,
    input rst_n,
    
    input rdy,
    input [N-1:0] mul1,//被乘数
    input [M-1:0] mul2,//乘数

    output rdy_out,
    output [N+M-1:0] res

);

wire [N+M-1:0] mul1_shift_top [M-1:0];
wire [M-1:0]   mul2_shift_top [M-1:0];
wire [N+M-1:0] mul_sum_next_top [M-1:0];
wire rdy_out_top [M-1:0];

mul_cell 
#(  .N(N),
    .M(M)
)
mul_cell0
(
    .clk   (clk),
    .rst_n (rst_n),
    .rdy   (rdy),
    .mul1  ({{M{1'b0}},mul1}),
    .mul2  (mul2),
    .mul_sum_last({(N+M){1'b0}}),
    //输出
    .rdy_out (rdy_out_top[0]),
    .mul1_shift (mul1_shift_top[0]),
    .mul2_shift (mul2_shift_top[0]),
    .mul_sum_next(mul_sum_next_top[0])
);

genvar i;
generate
    for(i=1;i<=M-1;i=i+1)begin:assembly_line
        mul_cell 
        #(  .N(N),
            .M(M)
        )
        mul_cell_uut
        (
            .clk   (clk),
            .rst_n (rst_n),
            .rdy   (rdy_out_top[i-1]),
            .mul1  (mul1_shift_top[i-1]),
            .mul2  (mul2_shift_top[i-1]),
            .mul_sum_last(mul_sum_next_top[i-1]),
            //输出
            .rdy_out (rdy_out_top[i]),
            .mul1_shift (mul1_shift_top[i]),
            .mul2_shift (mul2_shift_top[i]),
            .mul_sum_next(mul_sum_next_top[i])
        );
    end
endgenerate

assign rdy_out =rdy_out_top[M-1];
assign res     =mul_sum_next_top[M-1];
endmodule
