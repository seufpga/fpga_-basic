module mul_cell
#(
    parameter N=4,
    parameter M=4
)
(
    input clk  ,
    input rst_n,
    input rdy  ,

    input  [N+M-1:0] mul1 ,//被乘数
    input  [M-1:0]   mul2 ,//乘数
    input  [N+M-1:0] mul_sum_last,//上次累加值
    output reg rdy_out,
    output reg [N+M-1:0] mul1_shift,//被乘数移位值
    output reg [M-1:0]   mul2_shift,//乘数移位值
    output reg [N+M-1:0] mul_sum_next//本次累加结果
);

always @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        mul1_shift<=0;
        mul2_shift<=0;
        mul_sum_next<=0;
        rdy_out<=0; 
    end
    else begin
        if(rdy==1'b1)begin
            mul1_shift<=mul1<<1;
            mul2_shift<=mul2>>1;
            rdy_out<=1'b1;
            if(mul2[0]==1'b1)begin
                mul_sum_next<=mul_sum_last+mul1;
            end
            else begin
                mul_sum_next<=mul_sum_last;
            end
        end
        else begin
            mul1_shift<=0;
            mul2_shift<=0;
            mul_sum_next<=0;
            rdy_out<=0;
        end
    end
end


endmodule
