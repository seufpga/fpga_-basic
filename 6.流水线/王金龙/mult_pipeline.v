module mult_pipeline(clk,rst_n,in_rdy,data1,data2,result,out_rdy);

input clk,rst_n,in_rdy;
input [3:0] data1;
input [3:0] data2;
output reg [7:0] result;
output reg out_rdy;

wire [7:0] r1;
wire [7:0] r2;
wire [7:0] r3;
wire [7:0] r4;

reg [7:0] result_reg1;
reg [7:0] result_reg2;
reg out_rdy_reg;


assign r1=data1[0]?{4'b0,data2}:0;
assign r2=data1[1]?{3'b0,data2,1'b0}:0;
assign r3=data1[2]?{2'b0,data2,2'b0}:0;
assign r4=data1[3]?{1'b0,data2,3'b0}:0;

always @(posedge clk or negedge rst_n) begin

if(rst_n==0) begin
result_reg1<=0;
result_reg2<=0;
end
else if(in_rdy) begin
result_reg1<=r1+r2;
result_reg2<=r3+r4;
end
end


always @(posedge clk or negedge rst_n) begin

if(rst_n==0) begin
result<=0;
end
else if(out_rdy_reg) begin
result<= result_reg1 + result_reg2;
end
end

always @(posedge clk or negedge rst_n) begin
if(rst_n==0) begin
out_rdy <=   0;
out_rdy_reg<=0;
end
else begin
out_rdy_reg<=in_rdy;
out_rdy<=out_rdy_reg;
end
end

endmodule



