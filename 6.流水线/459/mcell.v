module mcell
(
    input   clk,
    input   rstn,
    input   en,
    input [7:0] mult1,
    input [3:0] mult2,
    input [7:0] mult1_temp,
    
    output reg [7:0] mult1_o,
    output reg [3:0] mult2_shift,
    output reg [7:0] mult1_reout,
    output reg       rdy
);

always@(posedge clk or negedge rstn)
    begin
        if(!rstn)
        begin   
            rdy           <= 1'b0;       
            mult1_o       <= 8'b0;
            mult1_reout   <= 8'b0;
            mult2_shift   <= 4'b0;
        end
        else if(en)
        begin
            rdy           <= 1'b1;
            mult2_shift   <= mult2>>1;
            mult1_o       <= mult1<<1;
            if(mult2[0]==1)
            begin
                mult1_reout <= mult1_temp+mult1;
            end
            else
            begin
                mult1_reout <= mult1_temp;
            end
        end
        else   
        begin
           rdy           <= 'b0; 
           mult1_o       <= 'b0; 
           mult1_reout   <= 'b0; 
           mult2_shift   <= 'b0; 
        end
    end
    
endmodule
            
            
            