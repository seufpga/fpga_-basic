module mult
(
    input       clk,
    input       rstn,
    input       data_rdy,
    input [3:0] mult1,
    input [3:0] mult2,

    output      res_rdy,
    output[7:0]      res,
    
    output[7:0]      res1,
    output [7:0]     res2,
    output [7:0]     res3
);

    wire[7:0]   mult1_t [3:0];
    wire[3:0]   mult2_t [3:0];
    wire[7:0]   mult1_acc_t [3:0];
    wire[3:0]   rdy_t;
    
    assign res1 = mult1_acc_t[0];
    assign res2 = mult1_acc_t[1];
    assign res3 = mult1_acc_t[2];
    
    
    mcell    mult_inst0
    (
       .clk        (clk) ,
       .rstn       (rstn) ,
       .en         (data_rdy) ,
       .mult1      ({{(4){1'b0}},mult1}) ,
       .mult2      (mult2) ,
       .mult1_temp ({(8){1'b0}}),
       
       .rdy        (rdy_t[0]) ,
       .mult1_o    (mult1_t[0]),
       .mult2_shift(mult2_t[0]),
       .mult1_reout(mult1_acc_t[0])
    );
       
    genvar       i;
    generate
        for(i=1;i<=3;i=i+1)
            begin:sbstep
            mcell  mult_instep
            (
               .clk        (clk) ,
               .rstn       (rstn) ,
               .en         (rdy_t[i-1]) ,
               .mult1      (mult1_t[i-1]) ,
               .mult2      (mult2_t[i-1]) ,
               .mult1_temp (mult1_acc_t[i-1]),
               
               .mult1_reout(mult1_acc_t[i]) , 
               .mult1_o    (mult1_t[i]),
               .mult2_shift(mult2_t[i]),
  
               .rdy        (rdy_t[i])               
            );
        end
    endgenerate
    
    assign res_rdy = rdy_t[3];
    assign res     = mult1_acc_t[3];
    
endmodule