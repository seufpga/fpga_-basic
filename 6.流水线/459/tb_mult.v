`timescale 1ns/1ns
module tb_mult();

reg         clk         ;
reg         rst         ;
reg         rdy         ;
reg [3:0]   mult1       ;
reg [3:0]   mult2       ;
wire        res_rdy     ;
wire[7:0]   res         ;

wire[7:0]   res1        ;
wire[7:0]   res2        ;
wire[7:0]   res3        ;

initial 
begin    
    mult1 = 4'd5;
    mult2 = 4'd5;
    rst = 1'b1;
    clk = 1'b1;
    rdy = 1'b1;

    #20
    mult1 = 4'd6;
    mult2 = 4'd6; 
    #20
    mult1 = 4'd7;
    mult2 = 4'd7;     
    #20
    mult1 = 4'd8;
    mult2 = 4'd8;   
/*     mult1 = 4'd5;
    mult2 = 4'd5; */
end
/* 
always #20 mult1 <= {$random} % 8;

always #20 mult2 <= {$random} % 8; */

initial begin
    $timeformat(-9,0,"ns",6);
    $monitor("@time %t:%d*%d = %d    %d  %d   %d",$time,mult1,mult2,res,res1,res2,res3);
end
/* initial
begin
    mult1 = 4'd5;
    mult2 = 4'd5;
    #20
    mult1 = 4'd5;
    mult2 = 4'd6; 
    #20
    mult1 = 4'd6;
    mult2 = 4'd6;     
    #20
    mult1 = 4'd3;
    mult2 = 4'd8;     
end */


always #10 clk = ~clk;

mult  mult_inst
(
    .clk        (clk),
    .rstn       (rstn),
    .data_rdy   (rdy),
    .mult1      (mult1),
    .mult2      (mult2),
                  
    .res_rdy    (res_rdy),
    .res        (res),
    
    .res1 (res1),
    .res2 (res2),
    .res3 (res3)
);

endmodule