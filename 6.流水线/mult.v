/*
模块：流水线乘法器主体
作用：实现流水线乘法器，输出结果。
作者：guzijie
日期：2022.2.8
*/

module mult
#(
	parameter M=4,
	parameter N=4
)
(
	input					sys_clk,
	input					sys_rst_n,
	input 				in_rdy,
	input[M-1:0]		mult_data1,
	input[N-1:0]		mult_data2,
	
	output				out_rdy,
	output[M+N-1:0] 	mult_result
	
);

//wire define
wire 				rdy_t[N-1:0];
wire[M+N-1:0]	data1_t[N-1:0];
wire[N-1:0]		data2_t[N-1:0];
wire[M+N-1:0] 	acc_t[N-1:0];

//initial
mult_cell #(.M(4),.N(4)) mult_cell_inst0(
	.sys_clk(sys_clk),
	.sys_rst_n(sys_rst_n),
	
	.in_rdy(in_rdy),
	.data1({{(N){1'b0}},mult_data1}),
	.data2(mult_data2),
	.acc_data({(M+N){1'b0}}),
	
	.out_rdy(rdy_t[0]),
	.data1_shift(data1_t[0]),
	.data2_shift(data2_t[0]),
	.acc_result(acc_t[0])

);

//generate
genvar i;
generate
	for(i=1;i<M;i=i+1) begin:mult_cell_step
		mult_cell #(.M(4), .N(4)) mult_cell_inst(
			.sys_clk(sys_clk),
			.sys_rst_n(sys_rst_n),
			
			.in_rdy(rdy_t[i-1]),	
			.data1(data1_t[i-1]),
			.data2(data2_t[i-1]),
			.acc_data(acc_t[i-1]),
			
			.out_rdy(rdy_t[i]),
			.data1_shift(data1_t[i]),
			.data2_shift(data2_t[i]),
			.acc_result(acc_t[i])
			
		);
	end
endgenerate

//output result
assign out_rdy = rdy_t[N-1];
assign mult_result = acc_t[N-1];


endmodule