//实现N位二进制超前进位加法电路，输出结果。
module adder
#(
	parameter   N = 8
)
(
	input    wire    [N-1:0]    X  ,
	input    wire    [N-1:0]    Y  ,
	
	output   wire    [N-1:0]    sum,
	output   wire               C  
);

assign {C, sum} = X + Y ;

endmodule