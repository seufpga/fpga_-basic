`timescale 1ns/1ns

module tb_adder();

reg   [7:0]  X;
reg   [7:0]  Y;
wire  [7:0]  sum;
wire         C;
wire  [8:0]  result;

initial
	begin
		X <= 8'd10; //0000_1010
		Y <= 8'd10; 
		#20 
		X <= 8'd200; //1100_1000
		Y <= 8'd200;
	end
	
assign	result = {C,sum};

adder
#(.N(8)
) adder_inst
(
	.X(X),
	.Y(Y),
	.C(C),
	.sum(sum)
);

endmodule

