module Adder
#(
    parameter N=8
)
(
    input clk              ,
    input rst_n            ,
    input [N-1:0] X        ,
    input [N-1:0] Y        ,
    output reg C           ,
    output reg [N-1:0] sum ,
    output reg [N:0] result
);


always @ *
    begin
        result=X+Y;
        sum=result[N-1:0];
        C=result[N];
    end

endmodule
