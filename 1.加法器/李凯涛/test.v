`timescale 1 ns/1 ns

module test();

//时钟和复位
reg clk  ;
reg rst_n;

//uut的输入信号
reg[7:0]  din0  ;
reg[7:0]  din1  ;
 //uut的输出信号
    wire      dout0;
    wire[7:0] dout1;
    wire[8:0] dout2;

        //时钟周期，单位为ns，可在此修改时钟周期。
        parameter CYCLE    = 20;

        //复位时间，此时表示复位3个时钟周期的时间。
        parameter RST_TIME = 3 ;

        //待测试的模块例化
        Adder uut(
            .clk        (clk     ), 
            .rst_n      (rst_n   ),
            .X          (din0    ),
            .Y          (din1    ),
            .C          (dout0   ),
            .sum        (dout1   ),
            .result     (dout2   )
            );


            //生成本地时钟50M
            initial begin
                clk = 0;
                forever
                #(CYCLE/2)
                clk=~clk;
            end

            //产生复位信号
            initial begin
                rst_n = 1;
                #2;
                rst_n = 0;
                #(CYCLE*RST_TIME);
                rst_n = 1;
            end

            //输入信号din0赋值方式
            initial begin
                #1;
                //赋初值
                din0 = 8'd10;
                din1 = 8'd10;
                #(1*CYCLE);
                din0 = 8'd200;
                din1 = 8'd200;
                #(5*CYCLE);
                $stop;
                //开始赋值
         
            end
            

          


            endmodule

