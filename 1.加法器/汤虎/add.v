/*
加法器
超前加法
v1.0
22.2.16
*/

module add
#(
    parameter Length = 8
)

(
    input wire [Length-1:0] X,
    input wire [Length-1:0] Y,
    output wire C_f,
    output wire [Length-1:0] sum,
    output reg [Length:0] result
);

assign{C_f,sum} = X+Y;
always @(*)
    result = X+Y;

endmodule

