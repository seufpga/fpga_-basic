`timescale 1 ns/1 ns

module tb_add();

parameter LENGTH = 8;

//时钟和复位
//reg clk  ;
//reg rst_n;

//uut的输入信号
reg [LENGTH-1:0] X ;
reg [LENGTH-1:0] Y ;


//uut的输出信号
wire      C_f;
wire[LENGTH-1:0] sum;
wire[LENGTH:0] result;

//时钟周期，单位为ns，可在此修改时钟周期。
parameter CYCLE    = 20;

//复位时间，此时表示复位3个时钟周期的时间。
parameter RST_TIME = 3 ;

//待测试的模块例化
add uut(
//    .clk          (clk     ), 
//    .rst_n        (rst_n   ),
    .X            (X       ),
    .Y            (Y       ),
    .C_f          (C_f     ),
    .sum          (sum     ),
    .result       (result  )
);


//生成本地时钟50M
/*initial begin
    clk = 0;
    forever
    #(CYCLE/2)
    clk=~clk;
end

//产生复位信号
initial begin
    rst_n = 1;
    #2;
    rst_n = 0;
    #(CYCLE*RST_TIME);
    rst_n = 1;
end
*/
//输入信号X赋值方式
initial begin
    #1;
    //赋初值
    X = 8'd10;
    #(10*CYCLE);
    X = 8'd200;
    #(30*CYCLE);        
    $stop;
    //开始赋值

end

//输入信号Y赋值方式
initial begin
    #1;
    //赋初值
    Y = 8'd10;
    #(10*CYCLE);
    Y = 8'd200;
    #(30*CYCLE);
    $stop;
    //开始赋值

end

endmodule

