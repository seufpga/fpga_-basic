module test1(X,Y,C,sum,result);
parameter N=8;

input [N-1:0] X,Y;
output reg [N-1:0]    sum;
output       reg C;
output reg [N:0]   result;

always @(*)
begin

result=X+Y;
sum=result[N-1:0];
C=result[N];

end

endmodule