/*
模块：加法器测试激励文件
作用：实现任意位二进制超前进位加法电路，输出结果。
作者：guzijie
日期：2022.2.6
*/
`timescale 1ns/1ns 

module adder_tb;

reg clk;
reg [7:0] X,Y;
wire[7:0] sum;
wire 			C;
wire[8:0] 	result;

initial begin
		clk = 1'b0;
		
		begin
		//正+正
			X <= 8'd10;
			Y <= 8'd10;
		#20 //有进位
			X <= 8'd200;
			Y <= 8'd200;
		end
		
end 
	
always #10 clk = ~clk; 

assign	result = {C,sum};


adder#(.N(8)) adder_inst(
	.X(X),
	.Y(Y),
	.C(C),
	.sum(sum)
);

endmodule