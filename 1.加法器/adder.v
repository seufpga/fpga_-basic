/*
模块：加法器
作用：实现任意位二进制超前进位加法电路，输出结果。
作者：guzijie
日期：2022.2.6
*/
module adder
#(
	parameter N = 8//width 
)
(
	input [N-1:0] 		X,Y,
	output				C,
	output[N-1:0]		sum
);

assign {C,sum}=	X+Y;


endmodule