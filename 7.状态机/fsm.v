/*
模块：有限状态机
作用：实现三段式有限状态机，结合状态转移图输出结果。
作者：guzijie
日期：2022.2.8
*/
module fsm(
	input sys_clk,
	input sys_rst_n,
	input	t,
	
	output reg[1:0] state,
	output reg		 happy
);

//state define
parameter FREE = 2'b00,
			 STUDY= 2'b01,
			 PLAY = 2'b10,
			 SLEEP= 2'b11;
			 
//(1)state_next variable
reg[1:0] state_next;	
always@(posedge sys_clk or negedge sys_rst_n) begin
	if(!sys_rst_n) begin
		state <= 2'b0;
	end
	else 
		state <= state_next;
end
		 
//(2)state machine:state shift
always@(*) begin
	if(!sys_rst_n) begin
		state_next <= 2'b0;
	end
	else begin
		case(state)
			FREE:
				state_next <= t?STUDY:FREE;
			STUDY:
				state_next <= t?PLAY:STUDY;
			PLAY:
				state_next <= t?SLEEP:PLAY;
			SLEEP:
				state_next <= t?FREE:SLEEP;
			default:
				state_next <= FREE;
		endcase
	end
end	

//(3)output
always@(posedge sys_clk or negedge sys_rst_n) begin
	if(!sys_rst_n) begin
		happy <= 1'b0;
	end
	else 
		happy <= (state_next == STUDY)?1'b1:1'b0;//注意是state_next;
end

endmodule		 
			 