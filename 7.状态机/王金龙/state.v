module state(clk,rst_n,t,sta,happy);
input clk,rst_n,t;
output reg [1:0]sta;
output reg happy;
parameter s0=2'd0,s1=2'd1,s2=2'd2,s3=2'd3;
reg [1:0] cur_sta,next_sta;

always @(posedge clk or negedge rst_n) begin

if(rst_n==0) begin
cur_sta=s0;
end
else begin
cur_sta<=next_sta;
end
end

always @(cur_sta or t) begin
case(cur_sta)

s0: begin
sta=s0;
happy=0;
if(t==1) begin 
next_sta<=s1;
end
else begin 
next_sta<=s0;
end
end

s1:begin
sta=s1;
happy=1;
if(t==1) begin
next_sta<=s2;
end
else begin
next_sta<=s1;
end
end

s2:begin
sta=s2;
happy=0;
if(t==1) begin
next_sta<=s3;
end
else begin
next_sta<=s2;
end
end

s3:begin
sta=s3;
happy=0;
if(t==1) begin
next_sta<=s0;
end
else begin
next_sta<=s3;
end
end

default: next_sta<=s0;


endcase 

end

endmodule