`timescale 1ns/1ns;
module state_tb;
reg clk,rst_n,t;
wire[1:0]sta;
wire happy;

state init(
.clk(clk),
.rst_n(rst_n),
.t(t),
.sta(sta),
.happy(happy)

);

initial begin
clk=0;
rst_n=0;
t=1;
#10;
rst_n=1;
end

always #10 clk=~clk;

endmodule