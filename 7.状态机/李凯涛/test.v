`timescale 1 ns/1 ns

module test();

//时钟和复位
reg clk  ;
reg rst_n;

//uut的输入信号
reg   t  ;

    wire[1:0] state;
    wire      happy;


        //时钟周期，单位为ns，可在此修改时钟周期。
        parameter CYCLE    = 20;

        //复位时间，此时表示复位3个时钟周期的时间。
        parameter RST_TIME = 3 ;

        //待测试的模块例化
        fsm uut(
            .clk          (clk     ), 
            .rst_n        (rst_n   ),
            .t            (t   ),
            .state        (state    ),
            .happy        (happy   )
            );


            //生成本地时钟50M
            initial begin
                clk = 0;
                forever
                #(CYCLE/2)
                clk=~clk;
            end

           
                        //产生复位信号
                        initial begin
                            //刚开始要复位一下，使得state=FREE,开始进行状态机的跳转
                            rst_n = 1;
                            #2;
                            rst_n = 0;
                            #2;
                            rst_n = 1;
                        end


            initial begin
                t = 1;
                #(7*CYCLE);
                $stop;
            end

           







            endmodule

