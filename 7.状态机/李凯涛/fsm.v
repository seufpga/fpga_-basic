module fsm
(
    input clk,
    input rst_n,
    input t,
    output  [1:0] state,
    output reg happy
);

parameter FREE =2'b00;
parameter STUDY=2'b01;
parameter PLAY =2'b10;
parameter SLEEP=2'b11;

reg [1:0] state_c,state_n;
wire free2study;
wire study2play;
wire play2sleep;
wire sleep2free;

always@(posedge clk or negedge rst_n)begin
    if(!rst_n)begin
        state_c <= FREE;
    end
    else begin
        state_c <= state_n;
    end
end


always@(*)begin
    case(state_c)
        FREE:begin
            if(free2study)begin
                state_n <= STUDY;
            end
            else begin
                state_n <= state_c;
            end
        end
        STUDY:begin
            if(study2play)begin
                state_n<= PLAY;
            end
            else begin
                state_n <= state_c;
            end
        end
        PLAY:begin
            if(play2sleep)begin
                state_n <= SLEEP;
            end
            else begin
                state_n<= state_c;
            end
        end
        SLEEP:begin
            if(sleep2free)begin
                state_n<= FREE;
            end
            else begin
                state_n<= state_c;
            end
        end
        default:begin
            state_n <= FREE;
        end
    endcase
end
//ת������
assign free2study  = state_c==FREE && t;
assign study2play  = state_c==STUDY&& t;
assign play2sleep  = state_c==PLAY && t;
assign sleep2free  = state_c==SLEEP&& t;
assign state=state_c;

//���
always  @(posedge clk or negedge rst_n)begin
    if(!rst_n)begin
        happy <=1'b0;      
    end
    else if(state_n==STUDY)begin
        happy <= 1'b1;
    end
    else begin
        happy <= 1'b0;
    end
end




endmodule
