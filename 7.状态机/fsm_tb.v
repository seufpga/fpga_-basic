/*
模块：有限状态机仿真激励文件
作用：测试三段式有限状态机，结合状态转移图输出结果。
作者：guzijie
日期：2022.2.8
*/

`timescale 1ns/1ns

module fsm_tb;

reg sys_clk;
reg sys_rst_n;
reg t;


initial begin
	sys_clk   = 1'b0;
	sys_rst_n = 1'b1;
	t			<= 1'b1;

end

always #10 sys_clk = ~sys_clk;

wire[1:0] state;
wire		 happy;
fsm fsm_inst(
	.sys_clk(sys_clk),
	.sys_rst_n(sys_rst_n),
	.t(t),
	
	.state(state),
	.happy(happy)
);


endmodule