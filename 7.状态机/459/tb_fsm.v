`timescale 1 ns/1 ns

module tb_fsm();

//时钟和复位
reg clk  ;
reg rst_n;

//uut的输入信号
reg  t  ;

//uut的输出信号
wire [1:0] state;
wire  happy_flag;


//待测试的模块例化
fsm init(
    .sys_clk          (clk     ), 
    .sys_rst_n        (rst_n   ),
    .t                (t       ),
    .state            (state   ),
    .happy_flag       (happy_flag)
);


//生成本地时钟50M
initial begin
    clk = 0;
    forever
    #(5)
    clk=~clk;
end


//输入信号din0赋值方式
initial begin
    //赋初值
    rst_n = 0;
    #5
    rst_n = 1;
    t = 1;
end
endmodule

