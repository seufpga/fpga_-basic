/*
模块：乘法器测试激励文件
作用：实现任意位二进制超前进位乘法电路，输出结果。
作者：guzijie
日期：2022.2.6
*/
`timescale 1ns/1ns 

module multiplier_tb;

reg 			clk;
reg [7:0] 	X,Y;
wire[15:0] 	product;

initial begin
		clk = 1'b0;
		begin
		//正*正
			X <= 8'd10;
			Y <= 8'd10;
		#20 //有进位
			X <= 8'd200;
			Y <= 8'd200;
		end
		
end 
	
always #10 clk = ~clk; 


multiplier#(.N(8)) multiplier_inst(
	.X(X),
	.Y(Y),
	.product(product)
);

endmodule