`timescale 1ns/1ns
module tb_multiplier();

reg    [7:0]    X  ;
reg    [7:0]    Y  ;
wire   [15:0]   sum;

initial
	begin
		X <= 8'd10;
		Y <= 8'd10;
		#20
		X <= 8'd200;
		Y <= 8'd200;
	end
	
multiplier
#(
	.N(8)
)multiplier_inst
(
	.X   (X),
	.Y   (Y),
	
	.sum (sum)
);

endmodule

	
	