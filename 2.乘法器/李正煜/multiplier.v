//实现任意位二进制超前进位乘法电路，输出结果。
module multiplier
#(
	parameter N = 8
)
(
	input    wire   [N-1:0]    X   ,
	input    wire   [N-1:0]    Y   ,
	
	output   wire   [2*N-1:0]  sum //N位二进制相乘的结果为2N位
);

assign sum = X*Y;

endmodule