module multi(X,Y,product);

parameter N=8;


input [N-1:0] X,Y;
output[(2*N-1):0] product;

assign product=X*Y;

endmodule

