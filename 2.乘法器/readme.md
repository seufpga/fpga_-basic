### 乘法器

实现任意位二进制超前进位乘法电路，输出结果。

#### 模块框图
![输入图片说明](multiplier.png)

#### 波形图
![输入图片说明](C__Code_quartus_adder_C__Users_admin_Desktop_wavedrom.png)

### 提交规范

每位成员提交时先以自己名字新建一个文件夹，文件夹内上传.v文件，以及一个完整的WORD/PDF文件，包括如下内容：
- Verilog HDL编写的模块代码；
- 通过modelsim测试的testbench文件，结果需与波形图结果一致； 
- modelsim仿真结果波形图； 
- 模块的介绍和理解； 

文件夹内提供testbench，测试文件，要求新建工程，编写.v文件，完成测试；

### 提示
乘法器操作数均为无符号数。 
