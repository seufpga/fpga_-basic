/*
�˷���
v1.0
22.2.17
*/
module multiplier
#(
    parameter LENGTH = 8
)
(
    input wire [LENGTH-1:0] X,Y,
    output reg [2*LENGTH-1:0] result
);

always@(*)
    result = X*Y;

endmodule
