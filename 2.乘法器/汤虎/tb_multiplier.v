`timescale 1 ns/1 ns

module tb_multiplier();
parameter LENGTH = 8;
//时钟和复位
reg clk  ;
reg rst_n;

//uut的输入信号
reg[LENGTH-1:0]  din0  ;
reg[LENGTH-1:0]  din1  ;


//uut的输出信号
wire[2*LENGTH-1:0] dout1;


//时钟周期，单位为ns，可在此修改时钟周期。
parameter CYCLE    = 20;

//复位时间，此时表示复位3个时钟周期的时间。
parameter RST_TIME = 3 ;

//待测试的模块例化
multiplier uut(
//    .clk          (clk     ), 
//    .rst_n        (rst_n   ),
    .X         (din0    ),
    .Y         (din1    ),
    .result        (dout1   )

);

initial begin
    #1;
    //赋初值
    din0 = 8'd10;
    din1 = 8'd10;
    #(10*CYCLE);
    din0 = 8'd200;
    din1 = 8'd200;
    #(20*CYCLE);
    $finish;

end

endmodule

