module Multiplier
#(
    parameter N=8
)
(
    input clk              ,
    input rst_n            ,
    input [N-1:0] X        ,
    input [N-1:0] Y        ,
    output reg [N+N-1:0] result
);


always @ *
    begin
        result=X*Y;
    end

endmodule
