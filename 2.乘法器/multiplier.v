/*
模块：乘法器
作用：实现任意位二进制超前进位乘法电路，输出结果。
作者：guzijie
日期：2022.2.6
*/
module multiplier
#(
	parameter N = 8//width 
)
(
	input [N-1:0] 		X,Y,
	output[N+N-1:0]	product
);

assign product = X*Y;


endmodule