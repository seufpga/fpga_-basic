/*
模块：串口通信接收器测试模块
作用：实现单字节，可定义波特率异步串口通信接收器。
作者：guzijie
日期：2022.3.3
*/
`timescale 1ns/1ns

module tb_uart_rx;


reg sys_clk;
always #10 sys_clk = ~sys_clk;


reg sys_rst_n;
reg rx;

wire[7:0]    out_data;
wire 			 out_rdy;


//测试代码
initial begin
	sys_clk = 1'b0;
	sys_rst_n = 1'b0;
	rx <= 1'b1;
	#50
		sys_rst_n = 1'b1;
	#100
		rx <= 1'b0;//LOW
	#(20*5208)
		rx <= 1'b1;//BIT0
	#(20*5208)
		rx <= 1'b1;//BIT1
	#(20*5208)
		rx <= 1'b1;//BIT2
	#(20*5208)
		rx <= 1'b0;//BIT3
	#(20*5208)
		rx <= 1'b1;//BIT4
	#(20*5208)
		rx <= 1'b0;//BIT5
	#(20*5208)
		rx <= 1'b1;//BIT6
	#(20*5208)
		rx <= 1'b0;//BIT7
	#(20*5208)
		rx <= 1'b1;//HIGH
	#(20*5208)
		rx <= 1'b1;
end

uart_rx uart_rx_inst
(
	.sys_clk(sys_clk),
	.sys_rst_n(sys_rst_n),
	.rx(rx),
	
	.out_data(out_data),
	.out_rdy(out_rdy)
);
endmodule