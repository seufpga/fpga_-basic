`timescale 1ns/1ns
module tb_uart_tx();

reg         sys_clk;
always #10 sys_clk = ~sys_clk;
       
reg         sys_rst_n;
reg [7:0]   in_data;
reg         in_flag;

wire        tx;

initial begin 
    sys_clk = 1'b0;
    sys_rst_n <= 1'b0;
    in_data <= 8'd2;
    #20
    sys_rst_n <= 1'b1;
    #40
    in_data = 8'd3;
    in_flag = 1'b1;
    #20
    in_flag = 1'b0;
    #(5208*20*10)
    in_data = 8'd9;
    in_flag = 1'b1;
    #20
    in_flag = 1'b0;
    #(5208*20*10)
    in_data = 8'd7;
    in_flag = 1'b1;
    #20
    in_flag = 1'b0;
end

uart_tx urat_tx_inst(
    .sys_clk        (sys_clk)         ,
    .sys_rst_n      (sys_rst_n)       ,
    .in_data        (in_data)         ,
    .in_flag        (in_flag)         ,
                   
    .tx             (tx)
);

endmodule
    