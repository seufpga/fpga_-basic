module uart_rx
#(
    parameter UART_BPS = 'd9600,
    parameter CLK_FREQ = 'd50_000_000
)
(   
    input   wire        sys_clk     ,
    input   wire        sys_rst_n   ,
    input   wire        rx          ,
    
    output  reg  [7:0]  out_data    ,
    output  reg         out_rdy    
);
localparam BAUD_CNT = CLK_FREQ/UART_BPS;

reg         rx_reg1     ;
reg         rx_reg2     ;
reg         rx_reg3     ;
reg         start_flag  ;
reg         state       ;
reg         work_en     ;
reg         rx_flag     ;
reg         bit_flag    ;
reg [12:0]  baud_cnt    ;
reg [7:0]   rx_data     ;
reg [3:0]   bit_cnt     ;

always@(posedge sys_clk or negedge sys_rst_n)
    if(sys_rst_n == 1'b0)
        work_en <= 1'b0;
    else if(start_flag == 1'b1)
        work_en <= 1'b1;
    else if((bit_cnt == 4'd8)&&(bit_flag == 1'b1))
        work_en <= 1'b0;

always@(posedge sys_clk or negedge sys_rst_n)
    if(sys_rst_n == 1'b0)
        baud_cnt <= 12'b0;
    else if((baud_cnt == BAUD_CNT - 1)||(work_en == 1'b0))
        baud_cnt <= 12'b0;
    else if(work_en == 1'b1)
        baud_cnt <= baud_cnt + 1'b1;
        
        
always@(posedge sys_clk or negedge sys_rst_n)
    if(sys_rst_n == 1'b0)
        rx_reg1 <= 1'b1;
    else 
        rx_reg1 <= rx;
        
always@(posedge sys_clk or negedge sys_rst_n)
    if(sys_rst_n == 1'b0)
        rx_reg2 <= 1'b1;
    else 
        rx_reg2 <= rx_reg1;

always@(posedge sys_clk or negedge sys_rst_n)
    if(sys_rst_n == 1'b0)
        rx_reg3 <= 1'b1;
    else
        rx_reg3 <= rx_reg2;
        
always@(posedge sys_clk or negedge sys_rst_n)
    if(sys_rst_n == 1'b0)
        start_flag <= 1'b0;
    else if ((~rx_reg2)&&(rx_reg3))
        start_flag <= 1'b1;
    else 
        start_flag <= 1'b0;

always@(posedge sys_clk or negedge sys_rst_n)
    if(sys_rst_n == 1'b0)
        bit_flag <= 1'b0;
    else if(baud_cnt == 2*BAUD_CNT/3)
        bit_flag <= 1'b1;
    else
        bit_flag <= 1'b0;
        
always@(posedge sys_clk or negedge sys_rst_n)
    if(sys_rst_n == 1'b0)
        bit_cnt <= 4'b0;
    else if((bit_flag == 1'b1)&&(bit_cnt == 4'd8))
        bit_cnt <= 4'b0;
    else if (bit_flag == 1'b1)
        bit_cnt <= bit_cnt +1'b1;
    else 
        bit_cnt <= bit_cnt;
        
always@(posedge sys_clk or negedge sys_rst_n)
    if(sys_rst_n == 1'b0)
        rx_data <= 8'b0;
    else if((bit_flag==1'b1)&&(work_en==1'b1))
        rx_data <= {rx_reg3,rx_data[7:1]};
    else 
        rx_data <= rx_data;

always@(posedge sys_clk or negedge sys_rst_n)
    if(sys_rst_n == 1'b0)
        rx_flag <= 1'b0;
    else if((bit_cnt == 4'd8)&&(bit_flag == 1'b1))
        rx_flag <= 1'b1;
    else 
        rx_flag <= 1'b0;

always@(posedge sys_clk or negedge sys_rst_n)
    if(sys_rst_n == 1'b0)
        out_data <= 8'b0;
    else if(rx_flag == 1'b1)
        out_data <= rx_data;
    else   
        out_data <= out_data;
        
always@(posedge sys_clk or negedge sys_rst_n)
    if(sys_rst_n == 1'b0)
        out_rdy <= 1'b0;
    else if(rx_flag == 1'b1)
        out_rdy <= 1'b1;
    else 
        out_rdy <= 1'b0;
        
endmodule
    
    
    
    