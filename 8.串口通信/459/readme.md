# uart_rx RTL    

![uart_rx](rtl_rx.png)

# uart_rx wave
![wave_rx](wave_rx.png)

# uart_tx RTL    

![uart_tx](rtl_tx.png)

# uart_tx wave
![wave_tx](wave_tx.png)
