`timescale 1ns/1ns
module tb_uart_rx();

reg         sys_clk;
always #10  sys_clk = ~sys_clk;

reg         sys_rst_n;
reg         rx;

wire [7:0]  out_data;
wire        out_rdy;

initial begin
    sys_clk = 1'b0;
    sys_rst_n = 1'b0;
    rx <= 1'b1;
    #20
    sys_rst_n = 1'b1;
    #40
    rx_bit(8'd3);
    rx_bit(8'd7);
    #40
    rx_bit(8'd2);
    rx_bit(8'd8);
end

    
    
task rx_bit(input [7:0] data);
    integer i;
    for(i=0;i<10;i=i+1)
    begin
        case(i)
            0: rx <= 1'b0;
            1: rx <= data[0];
            2: rx <= data[1];
            3: rx <= data[2];
            4: rx <= data[3];
            5: rx <= data[4];
            6: rx <= data[5];
            7: rx <= data[6];
            8: rx <= data[7];
            9: rx <= 1'b1;
        endcase
         #(5208*20);
    end
endtask  
uart_rx urat_rx_inst(
    .sys_clk      (sys_clk)       , 
    .sys_rst_n    (sys_rst_n)     ,
    .rx           (rx)            ,
                                  
    .out_data     (out_data)      ,
    .out_rdy      (out_rdy)
);

endmodule
    