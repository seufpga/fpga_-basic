/*
模块：串口通信发送器测试模块
作用：实现单字节，可定义波特率异步串口通信发送器。
作者：guzijie
日期：2022.3.3
*/
`timescale 1ns/1ns

module tb_uart_tx;


reg sys_clk;
always #10 sys_clk = ~sys_clk;


reg 			 sys_rst_n;
reg			 in_rdy;

wire		    tx;
wire 			 out_rdy;


//测试代码
initial begin
	sys_clk = 1'b0;
	sys_rst_n = 1'b0;
	in_rdy <= 1'b0;
	
	#50
		sys_rst_n <= 1'b1;
	#20
		in_rdy <= 1'b1;
	#20
		in_rdy <= 1'b0;
end

uart_tx
#(
	.UART_BPS ('d9600),//串口波特率
	.CLK_FREQ ('d50_000_000)//时钟频率
)
uart_tx_inst
(
	.sys_clk(sys_clk),
	.sys_rst_n(sys_rst_n),
	.in_data(8'd55),
	.in_rdy(in_rdy),
	
	.tx(tx),
	.out_rdy(out_rdy)
);

endmodule