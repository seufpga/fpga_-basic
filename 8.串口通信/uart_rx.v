/*
模块：串口通信接收器
作用：实现单字节，可定义波特率异步串口通信接收器。
作者：guzijie
日期：2022.3.3
*/
module uart_rx
#(
	parameter UART_BPS = 'd9600,//串口波特率
	parameter CLK_FREQ = 'd50_000_000//时钟频率
)
(
	input 				sys_clk,
	input 				sys_rst_n,
	input 				rx,
	
	output reg[7:0]	out_data,
	output reg		 	out_rdy
);

//localparam define
localparam BAUD_CNT_MAX = CLK_FREQ/UART_BPS;

//define states
reg[11:0] state,state_next;
parameter IDLE  = 12'b0000_0000_0000,
			 LOW   = 12'b0000_0000_0001,
			 BIT0  = 12'b0000_0000_0010,
			 BIT1  = 12'b0000_0000_0100,
			 BIT2  = 12'b0000_0000_1000,
			 BIT3  = 12'b0000_0001_0000,
			 BIT4  = 12'b0000_0010_0000,
			 BIT5  = 12'b0000_0100_0000,
			 BIT6  = 12'b0000_1000_0000,
			 BIT7  = 12'b0001_0000_0000,
			 HIGH  = 12'b0010_0000_0000,
			 ERROR = 12'b0100_0000_0000;
//reg1 打第一拍
reg rx_reg1;
always@(posedge sys_clk or negedge sys_rst_n) begin
	if(!sys_rst_n) 
		rx_reg1 <= 1'b1;
	else
		rx_reg1 <= rx;
end

//reg2 打第二拍
reg rx_reg2;
always@(posedge sys_clk or negedge sys_rst_n) begin
	if(!sys_rst_n) 
		rx_reg2 <= 1'b1;
	else
		rx_reg2 <= rx_reg1;
end

//reg3 提取下降沿
reg rx_reg3;
always@(posedge sys_clk or negedge sys_rst_n) begin
	if(!sys_rst_n) 
		rx_reg3 <= 1'b1;
	else
		rx_reg3 <= rx_reg2;
end

//下降沿
wire start_nedge;
assign start_nedge = (rx_reg2==1'b0 && rx_reg3==1'b1);

//波特计数器
reg[31:0] baud_cnt;
always@(posedge sys_clk or negedge sys_rst_n) begin
	if(!sys_rst_n) 
		baud_cnt <= 32'b0;
	else if(baud_cnt == BAUD_CNT_MAX-1 || state_next == IDLE)
		baud_cnt <= 32'b0;
	else 
		baud_cnt <= baud_cnt+32'b1;
end

//----------state machine------------------
always@(posedge sys_clk or negedge sys_rst_n) begin
	if(!sys_rst_n) 
		state <= 12'b0;
	else
		state <= state_next;
end



always@(*) begin
	if(!sys_rst_n)
		state_next <= 12'b0;
	else begin
		case(state)
			IDLE:
				state_next <=start_nedge?LOW:IDLE;
			LOW:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?BIT0:LOW;
			BIT0:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?BIT1:BIT0;
			BIT1:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?BIT2:BIT1;
			BIT2:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?BIT3:BIT2;
			BIT3:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?BIT4:BIT3;
			BIT4:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?BIT5:BIT4;
			BIT5:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?BIT6:BIT5;
			BIT6:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?BIT7:BIT6;
			BIT7:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?HIGH:BIT7;
			HIGH:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?IDLE:HIGH;
			default:
				state_next <= IDLE;
		endcase
	end
end
//


//rx_data
reg[7:0] rx_data;
always@(posedge sys_clk or negedge sys_rst_n) begin
	if(!sys_rst_n) 
		rx_data <= 8'b0;
	else begin
		case(state)
			BIT0:
				rx_data <= (baud_cnt==BAUD_CNT_MAX/2-1)?{rx_reg3,rx_data[7:1]}:rx_data;
			BIT1:
				rx_data <= (baud_cnt==BAUD_CNT_MAX/2-1)?{rx_reg3,rx_data[7:1]}:rx_data;
			BIT2:
				rx_data <= (baud_cnt==BAUD_CNT_MAX/2-1)?{rx_reg3,rx_data[7:1]}:rx_data;
			BIT3:
				rx_data <= (baud_cnt==BAUD_CNT_MAX/2-1)?{rx_reg3,rx_data[7:1]}:rx_data;
			BIT4:
				rx_data <= (baud_cnt==BAUD_CNT_MAX/2-1)?{rx_reg3,rx_data[7:1]}:rx_data;
			BIT5:
				rx_data <= (baud_cnt==BAUD_CNT_MAX/2-1)?{rx_reg3,rx_data[7:1]}:rx_data;
			BIT6:
				rx_data <= (baud_cnt==BAUD_CNT_MAX/2-1)?{rx_reg3,rx_data[7:1]}:rx_data;
			BIT7:
				rx_data <= (baud_cnt==BAUD_CNT_MAX/2-1)?{rx_reg3,rx_data[7:1]}:rx_data;
			default:
				rx_data <= rx_data;
		endcase
	end
end


//接受结束,进行输出
always@(posedge sys_clk or negedge sys_rst_n) begin
	if(!sys_rst_n) begin
		out_rdy  <= 1'b0;
		out_data <= 1'b0;
	end
	else if(state == HIGH && baud_cnt == BAUD_CNT_MAX-1) begin
		out_rdy	<= 1'b1;
		out_data <= rx_data;
	end
	else	
		out_rdy  <= 1'b0;
end

endmodule