/*
模块：串口通信发送器
作用：实现单字节，可定义波特率异步串口通信发送器。
作者：guzijie
日期：2022.3.4
*/
module uart_tx
#(
	parameter UART_BPS = 'd9600,//串口波特率
	parameter CLK_FREQ = 'd50_000_000//时钟频率
)
(
	input 				sys_clk,
	input 				sys_rst_n,
	input[7:0]			in_data,
	input					in_rdy,
	
	output reg			tx,
	output reg		 	out_rdy
);

//localparam define
localparam BAUD_CNT_MAX = CLK_FREQ/UART_BPS;

//define states
reg[11:0] state,state_next;
parameter IDLE  = 12'b0000_0000_0000,
			 LOW   = 12'b0000_0000_0001,
			 BIT0  = 12'b0000_0000_0010,
			 BIT1  = 12'b0000_0000_0100,
			 BIT2  = 12'b0000_0000_1000,
			 BIT3  = 12'b0000_0001_0000,
			 BIT4  = 12'b0000_0010_0000,
			 BIT5  = 12'b0000_0100_0000,
			 BIT6  = 12'b0000_1000_0000,
			 BIT7  = 12'b0001_0000_0000,
			 HIGH  = 12'b0010_0000_0000,
			 ERROR = 12'b0100_0000_0000;

//波特计数器
reg[31:0] baud_cnt;
always@(posedge sys_clk or negedge sys_rst_n) begin
	if(!sys_rst_n) 
		baud_cnt <= 32'b0;
	else if(baud_cnt == BAUD_CNT_MAX-1 || state == IDLE)
		baud_cnt <= 32'b0;
	else 
		baud_cnt <= baud_cnt+32'b1;
end

//----------state machine------------------
always@(posedge sys_clk or negedge sys_rst_n) begin
	if(!sys_rst_n) 
		state <= 12'b0;
	else
		state <= state_next;
end



always@(*) begin
	if(!sys_rst_n)
		state_next <= 12'b0;
	else begin
		case(state)
			IDLE:
				state_next <=in_rdy?LOW:IDLE;
			LOW:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?BIT0:LOW;
			BIT0:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?BIT1:BIT0;
			BIT1:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?BIT2:BIT1;
			BIT2:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?BIT3:BIT2;
			BIT3:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?BIT4:BIT3;
			BIT4:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?BIT5:BIT4;
			BIT5:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?BIT6:BIT5;
			BIT6:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?BIT7:BIT6;
			BIT7:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?HIGH:BIT7;
			HIGH:
				state_next <=(baud_cnt == BAUD_CNT_MAX-1)?IDLE:HIGH;
			default:
				state_next <= IDLE;
		endcase
	end
end
//


//tx
always@(posedge sys_clk or negedge sys_rst_n) begin
	if(!sys_rst_n) 
		tx <= 1'b1;
	else begin
		case(state)
			IDLE:
				tx <= in_rdy?1'b0:1'b1;
			LOW:
				tx <= (baud_cnt == BAUD_CNT_MAX-1)?in_data[0]:1'b0;
			BIT0:
				tx <= (baud_cnt == BAUD_CNT_MAX-1)?in_data[1]:in_data[0];
			BIT1:
				tx <= (baud_cnt == BAUD_CNT_MAX-1)?in_data[2]:in_data[1];
			BIT2:
				tx <= (baud_cnt == BAUD_CNT_MAX-1)?in_data[3]:in_data[2];
			BIT3:
				tx <= (baud_cnt == BAUD_CNT_MAX-1)?in_data[4]:in_data[3];
			BIT4:
				tx <= (baud_cnt == BAUD_CNT_MAX-1)?in_data[5]:in_data[4];
			BIT5:
				tx <= (baud_cnt == BAUD_CNT_MAX-1)?in_data[6]:in_data[5];
			BIT6:
				tx <= (baud_cnt == BAUD_CNT_MAX-1)?in_data[7]:in_data[6];
			BIT7:
				tx <= (baud_cnt == BAUD_CNT_MAX-1)?1'b1:in_data[7];
			default:
				tx <= 1'b1;
		endcase
	end
end


//接受结束,进行输出
always@(posedge sys_clk or negedge sys_rst_n) begin
	if(!sys_rst_n) begin
		out_rdy  <= 1'b0;
	end
	else if(state == HIGH && baud_cnt == BAUD_CNT_MAX-1) begin
		out_rdy	<= 1'b1;
	end
	else	
		out_rdy  <= 1'b0;
end

endmodule