module uart_rxd//数据接受模块
#(
    parameter SYSTEM_CLK = 50_000_000,//系统时钟
    parameter UART_BPS = 115_200      //串口波特率
)
(
    input clk,
    input rst_n,
    input rxd,
    output reg [8-1:0] uart_data,//输出接收到的数据
    output reg rxd_end
);

localparam ONE_DATA_CONT = SYSTEM_CLK/UART_BPS;//一个数据传输完需要的计数值
localparam ALL_DATA_CONT = 10;//8bit数据+1bit校验位+1bit终止位

reg rxd_trigger_1;
reg rxd_trigger_2;
reg [8:0] cnt0;
reg [3:0] cnt1;
reg [7:0] data_reg;
reg data_right;
reg odd;
reg rxd_flag;

wire rxd_start;

//下降沿检测
assign rxd_start = rxd_trigger_2 & (~rxd_trigger_1);
//将输入rxd数据延迟两个时钟周期，避免出现亚稳态
always @(posedge clk or negedge rst_n)begin
    if(!rst_n)begin
        rxd_trigger_1<=1'b0;
        rxd_trigger_2<=1'b0;
    end
    else begin
        rxd_trigger_1<=rxd;
        rxd_trigger_2<=rxd_trigger_1;
    end
end

//开始接受数据标志信号以及传输结束信号
always @(posedge clk or negedge rst_n)begin
    if(!rst_n)begin
        rxd_flag<=1'b0;
        rxd_end <=1'b0;
    end
    else begin
        if(rxd_start)begin
            rxd_flag<=1'b1;
            rxd_end<=1'b0;
        end
        else if((cnt1==ALL_DATA_CONT)&&(cnt0==(ONE_DATA_CONT/2)))begin//计数到停止位中间结束
            rxd_flag<=1'b0;
            rxd_end<=1'b1;
        end
        else begin
            rxd_flag<=rxd_flag;
            rxd_end<=1'b0;
        end
    end
end

//cnt0 1bit数据计数器 cnt1 接收数据个数计数器
always @(posedge clk or negedge rst_n)begin
    if(!rst_n)begin
        cnt0='b0;
        cnt1='b0;
    end
    else begin
        if(rxd_flag)begin
            if(cnt0<ONE_DATA_CONT-1)begin
                cnt0<=cnt0+1'b1;
                cnt1<=cnt1;
            end
            else begin
                cnt0<='b0;
                cnt1<=cnt1+1'b1;
            end
        end
        else begin
            cnt0='b0;
            cnt1='b0;
        end
    end
end
//将8bit数据和1bit校验位储存下来
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        data_reg<=8'b0;
    end
    else begin
        if(rxd_flag)begin
            if(cnt0==(ONE_DATA_CONT/2-1))begin
                case(cnt1)
                    4'd1:data_reg[0]<=rxd_trigger_2;
                    4'd2:data_reg[1]<=rxd_trigger_2;
                    4'd3:data_reg[2]<=rxd_trigger_2;
                    4'd4:data_reg[3]<=rxd_trigger_2;
                    4'd5:data_reg[4]<=rxd_trigger_2;
                    4'd6:data_reg[5]<=rxd_trigger_2;
                    4'd7:data_reg[6]<=rxd_trigger_2;
                    4'd8:data_reg[7]<=rxd_trigger_2;               
                    default:;
                endcase
            end
            else
                data_reg<=data_reg;
        end
        else
            data_reg<=8'b0;
    end
end

//对输入的数据进行校验
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        odd<=1'b1;
        data_right<=1'b0;
    end
    else begin
        if(cnt0==(ONE_DATA_CONT/2-1))begin
            if(cnt1>0&&cnt1<9)begin
                data_right<=1'b0;
                if(rxd_trigger_2)
                    odd<=~odd;
                else
                    odd<=odd;
            end
            else if(cnt1==9)begin
                if(odd==rxd_trigger_2)begin
                    data_right<=1'b1;
                    odd<=1'b1;
                end
                else begin
                    data_right<=1'b0;
                    odd<=1'b1;
                end
            end
            else begin
                odd<=odd;
                data_right<=1'b0;
            end
        end
        else begin
            odd<=odd;
            data_right<=1'b0;
        end
    end
end


//输出数据
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        uart_data<=8'b0;
    end
    else begin
        if(data_right==1)begin
            uart_data<=data_reg;
        end
        else
            uart_data<=uart_data;
    end
end


endmodule
