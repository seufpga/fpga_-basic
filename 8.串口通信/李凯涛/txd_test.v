`timescale 1ns/1ns	//定义时间刻度
//模块、接口定义
module txd_test();
 
reg 			sys_clk			;			
reg 			sys_rst_n		;			
reg [7:0]		uart_tx_data	;
reg 			uart_tx_en		;
			
wire 	 		uart_txd		;
 
parameter		BPS 	= 'd230400		;			//波特率
parameter		CLK_FRE = 'd50_000_000	;			//系统频率50M
//例化发送模块
uart_txd #(
	.SYSTEM_CLK		(CLK_FRE	),		//波特率9600
	.UART_BPS 		(BPS		)		//时钟频率50M	
)	
u_uart_tx(	
	.clk		(sys_clk		),			
	.rst_n		(sys_rst_n		),
	
	.data_in	(uart_tx_data	),			
	.data_rdy		(uart_tx_en		),		
	.txd		(uart_txd		)	
);
 
localparam	BIT_TIME = 'd1000_000_000 / BPS ;	//计算出传输每个bit所需要的时间
 
initial begin	
	sys_clk <=1'b0;	
	sys_rst_n <=1'b0;		
	uart_tx_en <=1'b0;
	uart_tx_data <=8'd0;				
	#80 										//系统开始工作
		sys_rst_n <=1'b1;
	#3000
		uart_tx_en <=1'b1;	
		uart_tx_data <= ({$random} % 256);		//发送8位随机数据
		#20	uart_tx_en <=1'b0;	
	#(BIT_TIME * 11)							//发送1个BYTE需要10个bit
	#2000										//多延迟一些为了两次发送的仿真波形看起来比较清楚
		uart_tx_en <=1'b1;	
		uart_tx_data <= ({$random} % 256);		//发送8位随机数据
		#20	uart_tx_en <=1'b0;		
end
 
always #10 sys_clk=~sys_clk;					//时钟20ns,50M
 
endmodule 

