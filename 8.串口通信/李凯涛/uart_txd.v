module uart_txd
#(
    parameter SYSTEM_CLK = 50_000_000,//系统时钟
    parameter UART_BPS = 115_200      //串口波特率
)
(
    input clk,
    input rst_n,
    input [8-1:0] data_in,
    input data_rdy,
    output reg txd
);

localparam ONE_DATA_CONT = SYSTEM_CLK/UART_BPS;//一个数据传输完需要的计数值
localparam ALL_DATA_CONT = 10;//8bit数据+1bit校验位+1bit终止位

reg data_rdy_1;
reg data_rdy_2;
reg [7:0] uart_txd_reg;
reg [8:0] cnt0;
reg [3:0] cnt1;
reg txd_flag;
reg odd;

wire txd_start;

//检测数据发送上升沿
assign txd_start = (~data_rdy_2) &data_rdy_1;

always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        data_rdy_1<=1'b0;
        data_rdy_2<=1'b0;
    end
    else begin
        data_rdy_1<=data_rdy;
        data_rdy_2<=data_rdy_1;
    end
end
//发送标志信号
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        txd_flag<=1'b0;
    end
    else begin
        if(txd_start)
            txd_flag<=1'b1;
        else if((cnt1==ALL_DATA_CONT)&&(cnt0==(ONE_DATA_CONT/2)))
            txd_flag<=1'b0;
        else
            txd_flag<=txd_flag;
    end
end

//cnt0 1bit数据计数器 cnt1 接收数据个数计数器
always @(posedge clk or negedge rst_n)begin
    if(!rst_n)begin
        cnt0='b0;
        cnt1='b0;
    end
    else begin
        if(txd_flag)begin
            if(cnt0<ONE_DATA_CONT-1)begin
                cnt0<=cnt0+1'b1;
                cnt1<=cnt1;
            end
            else begin
                cnt0<='b0;
                cnt1<=cnt1+1'b1;
            end
        end
        else begin
            cnt0='b0;
            cnt1='b0;
        end
    end
end
//对数据进行奇校验

always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        odd<=1'b0;
    end
    else begin
        odd<=~(^data_in);
    end
end
//将要发送的数据寄存下来
always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        uart_txd_reg<=8'b0;
    end
    else begin
        if(txd_start)
            uart_txd_reg<=data_in;
        else if((cnt1==ALL_DATA_CONT)&&(cnt0==(ONE_DATA_CONT/2)))
            uart_txd_reg<=8'b0;
        else 
            uart_txd_reg<=uart_txd_reg;
    end
end

always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        txd<=1'b1;
    end
    else begin
        if(txd_flag)begin
            case(cnt1)
                4'd0:txd<=1'b0;
                4'd1:txd<=uart_txd_reg[0];
                4'd2:txd<=uart_txd_reg[1];
                4'd3:txd<=uart_txd_reg[2];
                4'd4:txd<=uart_txd_reg[3];
                4'd5:txd<=uart_txd_reg[4];
                4'd6:txd<=uart_txd_reg[5];
                4'd7:txd<=uart_txd_reg[6];
                4'd8:txd<=uart_txd_reg[7];
                4'd9:txd<=odd;
                4'd10:txd<=1'b1;
                default:;
            endcase
        end
        else
            txd<=1'b1;
    end
end
endmodule
