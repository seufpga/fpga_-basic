### 总线

实现任意位总线操作，模块读取数据后根据信号判断是否进行+1再输出。

#### 模块框图
![输入图片说明](%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20220206201537.png)

#### 波形图
![输入图片说明](C__Code_quartus_mux_C__Code_quartus_mux_C__Code_quartus_mux_C__Code_quartus_comparator_C__Code_quartus_comparator_C__Code_quartus_comparator_comparator.png)

### 提交规范

每位成员提交时先以自己名字新建一个文件夹，文件夹内上传.v文件，以及一个完整的WORD/PDF文件，包括如下内容：
- Verilog HDL编写的模块代码；
- 通过modelsim测试的testbench文件，结果需与波形图结果一致； 
- modelsim仿真结果波形图； 
- 模块的介绍和理解； 

文件夹内提供testbench，测试文件，要求新建工程，编写.v文件，完成测试；

### 提示
三态门的实现，高阻态。

