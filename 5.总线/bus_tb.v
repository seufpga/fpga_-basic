/*
模块：总线测试激励文件
作用：实现任意位总线操作，模块读取数据后根据信号判断是否进行+1再输出。
作者：guzijie
日期：2022.2.6
*/
`timescale 1ns/1ns 

module bus_tb;

reg clk;
wire [11:0] databus;
reg  [11:0] data;
reg write;
reg linkbus;


initial begin
		clk = 1'b0;
		write = 1'b0;
		linkbus =1'b0;
		data =12'b0;
		begin
		#20 
			write 	= 1'b1;
		#20 
			write 	= 1'b0;
			linkbus 	= 1'b1;
		#20
			linkbus 	= 1'b0;
		end
		
end 

assign databus = (!linkbus)?data:12'hzzz;

always #10 clk = ~clk; 


bus#(.N(12)) bus_inst(
	.databus(databus),
	.linkbus(linkbus),
	.write(write)
);

endmodule