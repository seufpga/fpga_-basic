module bus(linkbus,write,databus);
parameter N=12;

input linkbus,write;
inout  [N-1:0] databus;
reg [N-1:0] data;
assign databus=(linkbus)?data:12'hzzz;
always @(posedge write)
begin
data=databus+1'd1;
end

endmodule 