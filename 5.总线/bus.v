/*
模块：总线
作用：实现总线的读写操作，模块读取数据后根据信号判断是否进行+1再输出。
作者：guzijie
日期：2022.2.6
*/
module bus
#(
	parameter N = 12//width 
)
(
	inout [N-1:0] 		databus,
	input					linkbus,//高电平有效
	input 				write		
);

reg [N-1:0]	outdata;

assign databus = linkbus? outdata:12'hzzz;//与width有关

always@(posedge write) begin
	outdata <= databus + 1'b1;
end




endmodule