`timescale 1 ns/1 ns

module tb_databus1();

//时钟和复位
reg clk  ;
reg rst_n;

//uut的输入信号
wire[11:0]  databus  ;
reg [11:0]  reg_data ;
reg write;
reg linkbus;

//时钟周期，单位为ns，可在此修改时钟周期。
parameter CYCLE    = 2;

//复位时间，此时表示复位3个时钟周期的时间。
parameter RST_TIME = 3 ;

//待测试的模块例化
databus1 init(
    .databus          (databus     ), 
    .write         (write   ),
    .linkbus         (linkbus    )
);
assign databus=(linkbus==0)?reg_data:12'hzzz;


//生成本地时钟50M
initial begin
    clk = 0;
    forever
    #(CYCLE/2)
    clk=~clk;
end

initial begin
    //赋初值
    write = 1'b0;
    linkbus = 1'b0;
    reg_data = 12'd0;
    #(CYCLE);
    write = 1'b1;
    #(CYCLE);
    write = 1'b0;
    linkbus = 1'b1;
    #(CYCLE);
    linkbus = 1'b0;
    //开始赋值

end


endmodule

