module databus1(
    input wire write,linkbus,
    inout [11:0] databus
);

reg [11:0] reg_data;

assign databus = linkbus?reg_data:12'bz;

always@(posedge write)
begin
    reg_data <= databus + 1'b1;
end

endmodule
