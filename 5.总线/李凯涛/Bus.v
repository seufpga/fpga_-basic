module Bus
#(parameter N=12)
(
    input         clk,
    input         rst_n,
    input         linkbus,
    input         write,
    inout  [N-1:0] databus
);

reg [N-1:0] data;

assign databus = linkbus? data:12'bz;

always  @(posedge clk or negedge rst_n)begin
    if(rst_n==1'b0)begin
        data<=12'b0;
    end
    else if(write==1'b1)begin
        data<=databus+1'b1;
    end
    else begin
        data<=databus;
    end
end
endmodule
