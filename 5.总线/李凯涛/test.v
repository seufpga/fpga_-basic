`timescale 1 ns/1 ns

module test();

//时钟和复位
reg clk  ;
reg rst_n;

//uut的输入信号
reg linkbus;
reg write;
reg[11:0] data;
//uut的输出信号
wire[11:0] databus;


        //时钟周期，单位为ns，可在此修改时钟周期。
        parameter CYCLE    = 20;

        //复位时间，此时表示复位3个时钟周期的时间。
        parameter RST_TIME = 3 ;

        //待测试的模块例化
        Bus uut(
            .clk        (clk     ), 
            .rst_n     (rst_n   ),
            .linkbus     (linkbus ),
            .write      (write   ),
            .databus     (databus )
            );


            //生成本地时钟50M
            initial begin
                clk = 0;
                forever
                #(CYCLE/2)
                clk=~clk;
            end

            //产生复位信号
            initial begin
                rst_n = 1;
            end

            //输入信号write赋值
            initial begin
                //赋初值
                write = 0;
                #(1*CYCLE);
                write = 1;
                #(1*CYCLE);
                write = 0;
                #(5*CYCLE);
                $stop;
            end
            //输入信号linkbus赋值
            initial begin
                //赋初值
                linkbus = 0;
                #(2*CYCLE);
                linkbus = 1;
                #(1*CYCLE);
                linkbus = 0;
                #(4*CYCLE);
                $stop;
            end
            //输入信号data赋值
            initial begin
                //赋初值
                data=12'd0;
                #(7*CYCLE);
                $stop;
            end

            assign databus=(!linkbus)?data:12'bz;

            


            endmodule

