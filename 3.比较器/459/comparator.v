module comparator
#(    parameter LENGTH = 8
)
(
    input wire [LENGTH-1:0] X,Y,
    output wire Equ,Gtr,Lss
);

assign Equ = (X==Y) ? 1'b1 : 1'b0,
    Gtr = (X>Y) ? 1'b1 : 1'b0,
    Lss = (X<Y) ? 1'b1 : 1'b0;

endmodule
