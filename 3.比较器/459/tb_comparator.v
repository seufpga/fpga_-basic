`timescale 1 ns/1 ns

module tb_comparator();

//时钟和复位
reg clk  ;
reg rst_n;

//uut的输入信号
reg[7:0]  din0  ;
reg[7:0]  din1  ;


//uut的输出信号
wire      dout0;
wire      dout1;
wire      dout2;


//时钟周期，单位为ns，可在此修改时钟周期。
parameter CYCLE    = 20;

//复位时间，此时表示复位3个时钟周期的时间。
parameter RST_TIME = 3 ;

//待测试的模块例化
comparator uut(
//    .clk          (clk     ), 
//    .rst_n        (rst_n   ),
    .X         (din0    ),
    .Y         (din1    ),
    .Equ        (dout0   ),
    .Gtr        (dout1   ),
    .Lss        (dout2   )

);

//输入信号din0赋值方式
initial begin
    //赋初值
    din0 = 8'd100;
    din1 = 8'd200;
    #(5*CYCLE);
    din0 = 8'd200;
    #(5*CYCLE);
    din1 = 8'd100;
    //开始赋值

end




endmodule

