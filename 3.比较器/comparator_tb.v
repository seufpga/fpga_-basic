/*
模块：比较器测试激励文件
作用：实现任意位二进制比较器电路，输出结果。
作者：guzijie
日期：2022.2.6
*/
`timescale 1ns/1ns 

module comparator_tb;

reg clk;
reg [7:0] X,Y;
wire XSY,XEY,XGY;

initial begin
		clk = 1'b0;
		
		begin
		//X<Y
			X <= 8'd100;
			Y <= 8'd200;
		#20 //X=Y
			X <= 8'd200;
			Y <= 8'd200;
		#20 //X>Y
			X <= 8'd200;
			Y <= 8'd100;
		end
		
end 
	
always #10 clk = ~clk; 


comparator#(.N(8)) comparator_inst(
	.X(X),
	.Y(Y),
	.XSY(XSY),
	.XEY(XEY),
	.XGY(XGY)
);

endmodule