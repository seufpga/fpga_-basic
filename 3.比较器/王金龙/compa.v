module compa (X,Y,XSY,XEY,XGY);
parameter N=8;

input  [N-1:0] X,Y;
output reg XSY,XEY,XGY;

always @(*)
begin

if(X==Y) begin
XEY<=1;
XSY<=0;
XGY<=0;
end

else if(X<=Y) begin
XEY<=0;
XSY<=1;
XGY<=0;
end

else if(X>=Y) begin
XEY<=0;
XSY<=0;
XGY<=1;
end

end

endmodule
