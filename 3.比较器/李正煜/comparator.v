module comparator
#(
	parameter N = 8
)
(
	input    wire   [N-1:0]  X,
	input    wire   [N-1:0]  Y,
	output	reg		       XSY, //x小于y
	output	reg		       XEY, //x等于y
	output	reg		       XGY  //x大于y
);

always@(*)
	begin
		if(X < Y) 
			begin
				XSY <= 1'b1;
				XEY <= 1'b0;
				XGY <= 1'b0;
			end
		else if(X == Y) 
			begin
				XSY <= 1'b0;
				XEY <= 1'b1;
				XGY <= 1'b0;
			end
		else  
			begin
				XSY <= 1'b0;
				XEY <= 1'b0;
				XGY <= 1'b1;
			end
	end


endmodule

