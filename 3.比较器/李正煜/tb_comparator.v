`timescale 1ns/1ns

module tb_comparator();

reg [7:0]  X;
reg [7:0]  Y;
wire     XSY;
wire     XEY;
wire     XGY;

initial 
	begin
		X <= 8'd50;
		Y <= 8'd60;//x<y
		#20 
		X <= 8'd50;
		Y <= 8'd50;//x=y
		#20 
		X <= 8'd60;
		Y <= 8'd50;//x>y
	end 
	
comparator
#(.N(8)
) comparator_inst
(
	.X(X),
	.Y(Y),
	.XSY(XSY),
	.XEY(XEY),
	.XGY(XGY)
);

endmodule

