`timescale 1 ns/1 ns

module test();

//时钟和复位
reg clk  ;
reg rst_n;

//uut的输入信号
reg[7:0]  X  ;
reg[7:0]  Y  ;

//uut的输出信号
    wire      XSY;
    wire      XEY;
    wire      XGY;

        //时钟周期，单位为ns，可在此修改时钟周期。
        parameter CYCLE    = 20;

        //复位时间，此时表示复位3个时钟周期的时间。
        parameter RST_TIME = 3 ;

        //待测试的模块例化
        Comparator uut(
            .clk        (clk  ), 
            .rst_n      (rst_n),
            .X          (X    ),
            .Y          (Y    ),
            .XSY        (XSY  ),
            .XEY        (XEY  ),
            .XGY        (XGY  )
            );


            //生成本地时钟50M
            initial begin
                clk = 0;
                forever
                #(CYCLE/2)
                clk=~clk;
            end

            //产生复位信号
            initial begin
                rst_n = 1;               
            end


            initial begin
 
                //赋初值
                X = 8'd100;
                #(1*CYCLE);
                X = 8'd200;
                #(5*CYCLE);
                $stop;

            end

            initial begin
 
                //赋初值
                Y = 8'd200;
                #(2*CYCLE);
                Y = 8'd100;
                #(4*CYCLE);
                $stop;

            end

          


            endmodule

