module Comparator
#(
    parameter N=8
)
(
    input clk,
    input rst_n,
    input [N-1:0] X,
    input [N-1:0] Y,
    output reg XSY,
    output reg XEY,
    output reg XGY
);

always @*
    begin
        if(X<Y)begin
            XSY=1;
            XEY=0;
            XGY=0;
        end
        else if(X==Y)begin
            XSY=0;
            XEY=1;
            XGY=0;
        end
        else begin
            XSY=0;
            XEY=0;
            XGY=1;
        end
    end
endmodule
