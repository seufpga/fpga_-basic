/*
模块：比较器
作用：实现任意位二进制数比较电路，输出结果。
作者：guzijie
日期：2022.2.6
*/
module comparator
#(
	parameter N = 8//width 
)
(
	input [N-1:0] 		X,Y,
	output	reg		XSY,XEY,XGY
);

always@(X or Y)begin
	if(X < Y) begin
		XSY <= 1'b1;
		XEY <= 1'b0;
		XGY <= 1'b0;
	end
	if(X == Y) begin
		XSY <= 1'b0;
		XEY <= 1'b1;
		XGY <= 1'b0;
	end
	if(X > Y) begin
		XSY <= 1'b0;
		XEY <= 1'b0;
		XGY <= 1'b1;
	end
		
end


endmodule