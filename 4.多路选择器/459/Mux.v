module Mux(
input [2:0] addr,
input nCS ,
input [7:0] in0,in1,in2,in3,in4,in5,in6,in7,
output reg [7:0] result
);
always@(*)
    if(!nCS)
       case(addr)
         3'b000: result <= in0;
         3'b001: result <= in1;
         3'b010: result <= in2;
         3'b011: result <= in3;
         3'b100: result <= in4;
         3'b101: result <= in5;
         3'b110: result <= in6;
         3'b111: result <= in7;
       endcase
    else 
        result <= 7'bzzzzzzzz;
endmodule
