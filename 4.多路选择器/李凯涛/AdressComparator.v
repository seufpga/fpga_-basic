module AdressComparator
#(
    parameter N=8
)
(
    input clk,
    input rst_n,
    input [2:0] address,
    input [N-1:0] in1,
    input [N-1:0] in2,
    input [N-1:0] in3,
    input [N-1:0] in4,
    input [N-1:0] in5,
    input [N-1:0] in6,
    input [N-1:0] in7,
    input [N-1:0] in8,
    output reg [N-1:0] result
);

always @ * begin
    case(address)
        3'd0:result=in1;
        3'd1:result=in2;
        3'd2:result=in3;
        3'd3:result=in4;
        3'd4:result=in5;
        3'd5:result=in6;
        3'd6:result=in7;
        3'd7:result=in8;
        default:result=in1;
    endcase
end
    endmodule
