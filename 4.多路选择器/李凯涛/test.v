`timescale 1 ns/1 ns

module test();


//时钟和复位
reg clk  ;
reg rst_n;

//uut的输入信号
reg [2:0]   address;
reg [7:0] in1;
reg [7:0] in2;
reg [7:0] in3;
reg [7:0] in4;
reg [7:0] in5;
reg [7:0] in6;
reg [7:0] in7;
reg [7:0] in8;

//uut的输出信号
wire[7:0] result;

        //时钟周期，单位为ns，可在此修改时钟周期。
        parameter CYCLE    = 20;

        //复位时间，此时表示复位3个时钟周期的时间。
        parameter RST_TIME = 3 ;

        //待测试的模块例化
        AdressComparator uut(
            .clk         (clk    ), 
            .rst_n       (rst_n  ),
            .address     (address),
            . in1        ( in1   ),
            . in2        ( in2   ),
            . in3        ( in3   ),
            . in4        ( in4   ),
            . in5        ( in5   ),
            . in6        ( in6   ),
            . in7        ( in7   ),
            . in8        ( in8   ),
            . result     (result )
            );


            //生成本地时钟50M
            initial begin
                clk = 0;
                forever
                #(CYCLE/2)
                clk=~clk;
            end

            //产生复位信号
            initial begin
                rst_n = 1;
               
            end

            
            initial begin
                in1=1;
                in2=2;
                in3=3;
                in4=4;
                in5=5;
                in6=6;
                in7=7;
                in8=8;
                #(9*CYCLE);
                $stop;
            end
            initial begin
               address=3'd0;
               #(1*CYCLE);
               address=3'd1;
               #(1*CYCLE);
               address=3'd2;
               #(1*CYCLE);
               address=3'd3;
               #(1*CYCLE);
               address=3'd4;
               #(1*CYCLE);
               address=3'd5;
               #(1*CYCLE);
               address=3'd6;
               #(1*CYCLE);
               address=3'd7;
                #(2*CYCLE);
                $stop;
            end




            endmodule

