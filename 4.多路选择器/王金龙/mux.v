module mux(addr,nCS,in1,in2,in3,in4,in5,in6,in7,in8,result);
parameter N=8;

input [2:0] addr;
input nCS;
input [N-1:0] in1,in2,in3,in4,in5,in6,in7,in8;
output reg [N-1:0] result;

always @(*)
begin

if(!nCS) begin
case (addr)
3'b000 : result=in1;
3'b001 : result=in2;
3'b010 : result=in3;
3'b011 : result=in4;
3'b100 : result=in5;
3'b101 : result=in6;
3'b110 : result=in7;
3'b111 : result=in8;
endcase
end
else result=0;

end

endmodule
