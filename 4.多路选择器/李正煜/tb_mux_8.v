`timescale 1ns/1ns

module tb_mux_8();

reg  [2:0]  addr;
reg  [7:0]  in0;
reg  [7:0]  in1;
reg  [7:0]  in2;
reg  [7:0]  in3;
reg  [7:0]  in4;
reg  [7:0]  in5;
reg  [7:0]  in6;
reg  [7:0]  in7; //8路输入
reg         nCS; //使能

wire [7:0]  out ;

initial
	begin
		nCS = 0;
		in0 = 8'd1;
		in1 = 8'd2;
		in2 = 8'd3;
		in3 = 8'd4;
		in4 = 8'd5;
		in5 = 8'd6;
		in6 = 8'd7;
		in7 = 8'd8;
	end
	
initial 
	begin
		addr = 3'b000;
		#10
		addr = 3'b001;
		#10
		addr = 3'b010;
		#10
		addr = 3'b011;
		#10
		addr = 3'b100;
		#10
		addr = 3'b101;
		#10
		addr = 3'b110;
		#10
		addr = 3'b111;
	end

mux_8
#(
	.N(8) //输入每路为8位
)mux_8_inst
(
	.addr(addr),
	.in0(in0),
	.in1(in1),
	.in2(in2),
	.in3(in3),
	.in4(in4),
	.in5(in5),
	.in6(in6),
	.in7(in7), //8路输入
	.nCS(nCS),//使能
	
	.out(out)
);

endmodule
