//实现8路任意位二进制选择器，输出结果。
module mux_8
#(
	parameter N = 8 //输入每路为8位
)
(
	input   wire  [2:0]    addr,
	input   wire  [N-1:0]  in0,
	input   wire  [N-1:0]  in1,
	input   wire  [N-1:0]  in2,
	input   wire  [N-1:0]  in3,
	input   wire  [N-1:0]  in4,
	input   wire  [N-1:0]  in5,
	input   wire  [N-1:0]  in6,
	input   wire  [N-1:0]  in7, //8路输入
	input   wire           nCS ,//使能
	
	output  reg   [N-1:0]  out
);
always@(*)
    if(!nCS) //低电平有效
       case(addr)
         3'b000: out = in0;
         3'b001: out = in1;
         3'b010: out = in2;
         3'b011: out = in3;
         3'b100: out = in4;
         3'b101: out = in5;
         3'b110: out = in6;
         3'b111: out = in7;
       endcase
    else 
       out = 0;
endmodule

